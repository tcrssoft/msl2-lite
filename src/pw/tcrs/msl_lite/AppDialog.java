package pw.tcrs.msl_lite;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;

/**
 * The dialog window used by McURL.
 */
public class AppDialog extends JFrame implements ActionListener {

    private final JPanel content;
    private final Launcher launcher;
    Timer timer;
    private int sec;
    JButton button;
    String text;

    public AppDialog(Launcher launcher, String text1, String text2) {
        this(launcher,text1,text2,0);
    }

    public AppDialog(Launcher launcher, String text1, String text2,int flag) {
        super("MSL2 Lite");
        this.launcher = launcher;

        content = new JPanel();
        content.setLayout(new BoxLayout(content, BoxLayout.Y_AXIS));
        content.setBorder(BorderFactory.createEmptyBorder(10, 10, 5, 10));
        setUndecorated(true);
        setLocationRelativeTo(null);
        setBackground(Color.white);
        setContentPane(content);

        JLabel title = new JLabel("MinecraftServerLife2 Lite");
        title.setFont(title.getFont().deriveFont(Font.BOLD, 14));
        JLabel label1 = new JLabel(text1);
        JLabel label2 = new JLabel(text2);
        button = new JButton(launcher == null ? "閉じる" : "起動");
        button.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                launchOrClose();
            }
        });
        button.setFocusable(false);
        JLabel credits = new JLabel("<html>MSL2 Lite \u00a9 2016 TCRS Soft.<br>原作: McURL 2.0 \u00a9 2013 by Tad Hardesty");

        title.setAlignmentX(0.5f);
        label1.setAlignmentX(0.5f);
        label2.setAlignmentX(0.5f);
        button.setAlignmentX(0.5f);
        credits.setAlignmentX(0.5f);

        timer = new Timer(1000 , this);

        content.add(title);
        content.add(Box.createVerticalStrut(5));
        content.add(label1);
        content.add(Box.createVerticalStrut(5));
        content.add(label2);
        content.add(Box.createVerticalStrut(5));
        content.add(button);
        content.add(Box.createVerticalStrut(5));
        content.add(credits);

        pack();
        Dimension size = new Dimension(getWidth(), button.getHeight());
        button.setMinimumSize(size);
        button.setPreferredSize(size);
        button.setMaximumSize(size);

        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
        setResizable(false);
        setVisible(true);
        sec = 5;
        if(flag==1){
            if (Main.exe.endsWith(".exe")) {
                String test=AppDialog.class.getProtectionDomain().getCodeSource().getLocation().getFile();
                StringBuilder sb = new StringBuilder(test);
                sb.deleteCharAt(0);
                File launcher_jar = new File(sb.toString()+"launcher.jar");
                File file=new File("");
                if(!checkBeforeReadfile(launcher_jar)){
                    JFileChooser filechooser = new JFileChooser("C:\\Program Files (x86)\\Minecraft\\game");
                    filechooser.setFileFilter(new FileNameExtensionFilter("*.jar", "jar"));
                    int selected = filechooser.showOpenDialog(this);
                    String a1="";
                    if (selected == JFileChooser.APPROVE_OPTION){
                        file  = filechooser.getSelectedFile();
                    }else if (selected == JFileChooser.CANCEL_OPTION){
                        a1="キャンセルされました";
                        label2.setText(a1);
                    }else if (selected == JFileChooser.ERROR_OPTION){
                        a1="エラー又は取消しがありました";
                        label2.setText(a1);
                    }
                    launcher_jar=file;
                }
                File outjar = new File(Launcher.getMinecraftDir(), "launcher.jar");
                try {
                    copyFile(launcher_jar,outjar);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                timer.start();
            }
        }else{
            timer.start();
        }
    }

    public static void copyFile(File in, File out) throws IOException {
        FileChannel inChannel = new FileInputStream(in).getChannel();
        FileChannel outChannel = new FileOutputStream(out).getChannel();
        try {
            inChannel.transferTo(0, inChannel.size(),outChannel);
        }
        catch (IOException e) {
            throw e;
        }
        finally {
            if (inChannel != null) inChannel.close();
            if (outChannel != null) outChannel.close();
        }
    }

    private boolean checkBeforeReadfile(File file){
        if (file.exists()){
            if (file.isFile() && file.canRead()){
                return true;
            }
        }

        return false;
    }

    public void actionPerformed(ActionEvent e){
        if (sec == 0){
            button.setText(text+"("+sec+")");
            timer.stop();
            launchOrClose();
        }else if(sec == 5){
            text=button.getText();
            button.setText(text+"("+sec+")");
            sec--;
        }else{
            button.setText(text+"("+sec+")");
            sec--;
        }
    }

    private void launchOrClose() {
        if (launcher != null) {
            dispose();
            launcher.startLauncher();
        } else {
            System.exit(0);
        }
    }

}