package pw.tcrs.msl_lite;

import javax.swing.*;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.util.Scanner;

/**
 * The main class for McURL.
 */
public class Main {

    private static String host;
    private static String port;
    public static String exe;

    public static void main(String[] args) throws MalformedURLException, URISyntaxException {
        System.out.println("MinecraftServerLife2 Lite");
        exe = Main.class.getProtectionDomain().getCodeSource().getLocation().toURI().getPath();
        if (exe.startsWith("/")) {
            exe = exe.substring(1);
        }

        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Exception e) {
            e.printStackTrace();
        }

        // argument size handling
        if (args.length == 0) {
            if (System.getenv("MCURL_WINDOWS") != null) {
                try {
                    windowsInstall();
                } catch (Exception e) {
                    e.printStackTrace();
                    new AppDialog(null, "インストールにおいて、予期しないエラーが発生しました。", e.toString());
                }
            } else {
                new AppDialog(null, "サーバーが指定されていません", "このソフトをインストールして、専用のリンクから起動できます。");
            }
            return;
        }
        if (args.length > 1) {
            System.out.println("余分なコマンドライン引数は無視します。");
        }

        // parse the url
        parseUrl(args[0]);
        if (host.equalsIgnoreCase("settings")) {
            new AppDialog(null, "minecraft://settings/", "MinecraftServerLife2 Liteに設定ページはありません。");
            return;
        }

        try {
            Launcher launcher = new Launcher(new String[]{"--server", host, "--port", port});
            if (!launcher.exists()) {
                new AppDialog(null, "ランチャーが見つかりません", "画面の指示に従ってください。",1);
            } else {
                new AppDialog(launcher, host + (port.equals("25565") ? "" : ":" + port), "このまま通常通りランチャーで起動してください。");
            }
        } catch (Exception e) {
            e.printStackTrace();
            new AppDialog(null, "予期しないエラーが発生しました", e.toString());
        }
    }

    private static void parseUrl(String text) {
        // Trim off leading minecraft: and slashes
        if (text.startsWith("minecraft:")) {
            text = text.substring(10);
        }else if(text.startsWith("minecraft:")){
            text = text.substring(10);
        }

        while (text.charAt(0) == '/') {
            text = text.substring(1);
        }

        // Trim everything after the first /
        int i = text.indexOf('/');
        if (i >= 0) {
            text = text.substring(0, i);
        }

        // Ignore possible usernames and passwords
        int at = text.indexOf('@');
        if (at >= 0) {
            text = text.substring(at + 1);
        }

        // find the host and port
        int colon = text.indexOf(':');
        if (colon >= 0) {
            host = text.substring(0, colon);
            port = text.substring(colon + 1);
        } else {
            host = text;
            port = "25565";
        }
    }

    private static void windowsInstall() throws Exception {
        // called when environment variable MCURL_WINDOWS is set
        // job is to install McURL to the Windows registry
        System.out.println("Performing Windows installation");

        exe = Main.class.getProtectionDomain().getCodeSource().getLocation().toURI().getPath();
        if (exe.startsWith("/")) {
            exe = exe.substring(1);
        }
        if (!exe.endsWith(".exe")) {
//            new AppDialog(null, "Installation failed", "You must install from a .exe file.");
            return;
        }
        exe = exe.replace("/", "\\");
        System.out.println("This file: " + exe);

        String root = "HKCR\\minecraft";
        String values[][] = {
                {root, "", "Minecraft Server (McURL) protocol"},
                {root, "URL Protocol", ""},
                {root + "\\DefaultIcon", "", exe + ",0"},
                {root + "\\shell", "", "open"},
                {root + "\\shell\\open", "", ""},
                {root + "\\shell\\open\\command", "", exe + " %1"}
        };

        Runtime rt = Runtime.getRuntime();
        for (String[] entry : values) {
            String key = entry[0];
            String value = entry[1];
            String data = entry[2];
            String cmd = "REG ADD " + key + " /f /v \"" + value + "\" /d \"" + data + "\"";
            System.out.println(cmd);

//            Process p = rt.exec(cmd);
//            Scanner err = new Scanner(p.getErrorStream());
//            int result = p.waitFor();

            String errorLine = "";
//            if (err.hasNextLine()) {
//                errorLine = err.nextLine();
//            }

//            if (errorLine.contains("Access is denied")) {
//                new AppDialog(null, "インストールに失敗しました。", "インストールするには管理者権限で起動する必要があります。");
//                return;
//            } else if (result != 0) {
//                new AppDialog(null, "インストールに失敗しました。", "レジストリエラー: " + errorLine);
//                return;
//            }
        }

        new AppDialog(null, "インストールに成功しました。", "専用のリンクをクリックしたら起動できます。");
    }

}
